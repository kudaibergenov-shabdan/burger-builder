import {nanoid} from "nanoid";
import './App.css';
import {useState} from 'react';
import IngredientContainer from "./components/IngredientContainer/IngredientContainer";
import meatImage from './assets/images/meat.png';
import cheeseImage from './assets/images/cheese.png';
import saladImage from './assets/images/salad.png';
import baconImage from './assets/images/bacon.png';
import BurgerContainer from "./components/BurgerContainer/BurgerContainer";

const INGREDIENTS = [
    {id: nanoid(), name: 'Meat', price: 50, image: meatImage, count: 0, disabledDeletion: true},
    {id: nanoid(), name: 'Cheese', price: 20, image: cheeseImage, count: 0, disabledDeletion: true},
    {id: nanoid(), name: 'Salad', price: 5, image: saladImage, count: 0, disabledDeletion: true},
    {id: nanoid(), name: 'Bacon', price: 30, image: baconImage, count: 0, disabledDeletion: true}
];

const App = () => {

    const [ingredients, setIngredients] = useState(INGREDIENTS);

    const [totalPrice, setTotalPrice] = useState(0);

    const getTotalIngredientsPrice = () => {
        const totalPrice = ingredients.reduce((accumulator, ingredient) => {
            accumulator += ingredient.price * ingredient.count;
            return accumulator;
        }, 0);
        return totalPrice;
    }

    const addIngredient = (ingredientName) => {
        setIngredients(ingredients.map(ingredient => {
            if (ingredient.name === ingredientName) {
                ingredient.count++;
                ingredient.disabledDeletion = false;
            }
            return ingredient;
        }));
        setTotalPrice(getTotalIngredientsPrice());
    };

    const removeIngredient = (ingredientName) => {
        setIngredients(ingredients.map(ingredient => {
            if (ingredient.name === ingredientName) {
                if (ingredient.count > 0) {
                    ingredient.count = 0;
                    ingredient.disabledDeletion = true;
                }
            }
            return ingredient;
        }));

        setTotalPrice(getTotalIngredientsPrice());
    };

    const ingredientContainerComponent = (
      <IngredientContainer
          ingredients={ingredients}
          addIngredient={addIngredient}
          removeIngredient={removeIngredient}
      />
    );

    const burgerContainerComponent = (
        <BurgerContainer
            ingredients={ingredients}
            addIngredient={addIngredient}
            totalPrice={totalPrice}/>
    )

    return (
        <div className="App">
            {ingredientContainerComponent}
            {burgerContainerComponent}
        </div>
    );
}

export default App;
