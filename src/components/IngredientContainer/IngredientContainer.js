import React from 'react';
import Ingredient from "../Ingredient/Ingredient";

const IngredientContainer = props => {

    const ingredientsComponent = props.ingredients.map((ingredient) => (
        <Ingredient
            key={ingredient.id}
            ingredient={ingredient}
            addIngredient={() => props.addIngredient(ingredient.name)}
            removeIngredient={() => props.removeIngredient(ingredient.name)}
        />
    ));

    return (
        <div className="box">
            <fieldset className="box-inner">
                <legend>Ingredients</legend>
                {ingredientsComponent}
            </fieldset>
        </div>
    );
};

export default IngredientContainer;