import React from 'react';
import Burger from "../Burger/Burger";

const BurgerContainer = props => {
    return (
        <div className="box">
            <fieldset className="box-inner">
                <legend>Burger Container</legend>
                <Burger ingredients={props.ingredients}/>
                <p>TotalPrice = {props.totalPrice}</p>
            </fieldset>
        </div>
    );
};

export default BurgerContainer;