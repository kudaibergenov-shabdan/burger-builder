import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './Ingredient.css';

const Ingredient = props => {
    return (
        <div className="ingredient-block">
            <button className="btn-ingredient ingredient-block-box" onClick={props.addIngredient}>
                <img className="ingredient-img" src={props.ingredient.image} alt={props.ingredient.name}/>
            </button>
            <span className="ingredient-block-box">{props.ingredient.name}</span>
            <span className="ingredient-block-box">x{props.ingredient.count}</span>
            <div className="ingredient-block-box ingredient-block-btn-delete">
                <button disabled={props.ingredient.disabledDeletion} className="btn-delete-ingredient" onClick={props.removeIngredient}>
                    <i className="far fa-trash-alt"></i>
                </button>
            </div>
        </div>
    );
};

export default Ingredient;