import React from 'react';

import './Burger.css';

const Burger = props => {
    const existingIngredients = props.ingredients.filter(ingredient => ingredient.count > 0);
    const allIngredients = [];
    existingIngredients.forEach(ingredient => {
        for(let i = 0; i < ingredient.count; i++) {
            allIngredients.push({id: ingredient.id + i, name: ingredient.name});
        }
    });
    const burgerIngredientsComponent = allIngredients.map(ingredient => (
        <div key={ingredient.id} className={ingredient.name}></div>
    ));

    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {burgerIngredientsComponent}
            <div className="BreadBottom"></div>
        </div>
    );
};

export default Burger;